import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import myStore from '../store/myStore'
import App from './App'

Vue.use(Vuex)
Vue.use(VueRouter)

const store = new Vuex.Store(myStore)

const routes = [
	{
		path: '/goods',
		component: require('./Goods')
	}
]

const router = new VueRouter({
	routes
})

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
