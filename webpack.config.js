var path = require("path")
var webpack = require('webpack')
const vuxLoader = require('vux-loader')

const webpackConfig = {
	entry: path.join(__dirname, '/src/main.js'),
  output: {
  	path: path.join(__dirname, '/dist/'),
		publicPath: '/assets/',    
    filename: 'bundle.js'
  },
	devServer: {
		inline: true
	},
  module: {
  	rules: [
			{
				test: /\.css$/,
		    use: [
       		"style-loader",
       		"css-loader",
      		"less-loader"
    		]
			},
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
　　　　　　test: /\.(png|jpg|gif)$/,
　　　　　　loader: 'url-loader?limit=8192'
　　　　}
    ]
  },
  resolve: {
    extensions: ['.js', '.json', '.vue', '.css'],
		alias: {
			'vue$': 'vue/dist/vue.common.js'
		}
  },
  resolveLoader: {
    moduleExtensions: ['-loader']
  },
	plugins: [
		// new webpack.optimize.UglifyJsPlugin(),
    new webpack.ProvidePlugin({
      'vue': 'vue'
    })
	]
}

module.exports = vuxLoader.merge(webpackConfig, {
  options: {},
  plugins: [
    {
      name: 'vux-ui'
    }
  ]
})